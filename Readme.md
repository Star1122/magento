Module installation :

We need to run magento commands to install module

    • php bin/magento setup:upgrade (this command will update database and at this time it will create new customer attribute `customer_status`).
    • php bin/magento s:d:c (this command will create and resolve dependency injection and compile code)
    • php bin/magento s:s:d -f (this command will create and deploy the static file )
    • php bin/magento c:f (this command will clean the magento cache)

After above commands we can login into customer panel and after click on status link we will see the form to change customer status.
