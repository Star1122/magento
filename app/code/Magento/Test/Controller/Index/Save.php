<?php

namespace Magento\Test\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $_customer_session;
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $_customerRepository;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\Session $customer_session,
        Context $context, PageFactory $pageFactory
    )
    {
        parent::__construct($context);
        $this->_customerRepository = $customerRepository;
        $this->_customer_session = $customer_session;
        $this->pageFactory = $pageFactory;
    }

    /**
     * Index Action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $customer_id = $this->_customer_session->getCustomer()->getId();
        $status = $this->getRequest()->getParam('status');
        if (!$status) {
            $this->messageManager->addNoticeMessage(__('status is required'));
            $this->_redirect($this->_redirect->getRefererUrl());
        }

        $customer = $this->_customerRepository->getById($customer_id);
        $customer->setCustomAttribute('customer_status', $status);
        if ($this->_customerRepository->save($customer)) {
            $this->messageManager->addSuccessMessage(__('we saved you new status, now you can see it at the top right'));
        } else {
            $this->messageManager->addErrorMessage(__('something going wrong. we could not save your status'));
        }
        $this->_redirect($this->_redirect->getRefererUrl());
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();
        return $resultPage;
    }
}
