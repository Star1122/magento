<?php
/**
 * Show
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magento\Test\Block\Status;

use Magento\Framework\View\Element\Template;

class Show extends Template
{
    private $_customer_session;

    public function __construct(
        \Magento\Customer\Model\Session $customer_session,
        Template\Context $context, array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_customer_session = $customer_session;
    }

    public function isLoggedIn(): bool
    {
        return $this->_customer_session->isLoggedIn();
    }

    /**
     * @return string
     */
    public function getCustomerStatus()
    {
        return $this->_customer_session->getCustomer()->getData('customer_status');
    }
}
